# MyNHS

Enables each NHS member to check their status within the Honor Society records using a relay somewhere in Miami.

## Why?

I get a lot of NHS-related emails. Sometimes, they'll ask me to check how much service they've done or how many meetings they've missed. I figured that this would be an easily automatable task.

## So, How Does It Work?

Our records are stored as Google Sheets: one for attendance and one for service hours.

The problem is that I can't really download those two sheets on the spot in my website because only a few people are supposed to have access to that information.

So, instead of that, I set up a [relay](https://gitlab.com/nbnhs/mynhs-updater) instead. This relay can:

* Get information about a member
* Send it to my website in comprehensible chunks, unlike Google
* Do it all securely, unlike if I were to store my Google credentials in the website itself

Once the user puts their ID in on the main page, that relay gets their info and that info is subsequently plugged into various places on the next page.

## Code Setup

The `Makefile` does whatever is needed to build the project:

* Installs `yarn` dependencies (note: requires `yarn`)
* Converts Markdown to HTML (must add rules manually in `Makefile` and create corresponding folders in `public`)
* Lints TypeScript
* Minifies TypeScript into JavaScript (must add new config object in `webpack.config.js` for each new folder you make in `src/js`).

Let's take you through the flow of execution:

1. User lands on `public/index.html`
2. JavaScript checks for a valid ID Card number cookie
    1. If the user **doesn't** have a valid cookie for an ID Card number, they are prompted for one
        1. ID Card number is saved as a cookie
        2. Service and attendance records are asynchronously loaded and used to asynchronously construct a `User` object
        3. The `User` object is serialized and put into `localStorage`
    2. If the user **does** have a valid cookie, it's assumed that the `User` object has been put into `localStorage`.
6. `public/app/index.html` is loaded
7. The `localStorage` object is deserialized into a `User` object
8. The page is filled with information obtained from the `User` object
