import User from "../User";

const u: User = User.deserialize(localStorage.getItem("userInfo"));

window.onload = () => {
  // replace everything
  // between the <a ...> </a> tags
  document.getElementById("name").innerHTML = u.name;
  if (!u.duesPaid) {
    document.getElementById("duespaid").innerHTML = "You haven't paid your dues yet!";
  }

  if (u.absencesLeft === 1) {
    document.getElementById(
      "remainingAbsences",
    ).innerHTML = "no";
  } else {
    document.getElementById(
      "remainingAbsences",
    ).innerHTML = u.absencesLeft.toString();
  }

  document.getElementById(
    "remainingService",
  ).innerHTML = u.serviceLeft.toString();

  document.getElementById("completedEvents").innerHTML =
    u.completedEventsAsHTML; // creates a unordered list in HTML

  if (u.contest) {
    document.getElementById("contest").innerHTML =
      "Note: some of your information is under review due to some mishap. We'll get to the bottom of this!";
  }

  if (u.probation) {
    document.getElementById("probation").innerHTML =
      `Note: You're under probation for not attending an event you signed up for.
      Doing this again will result in expulsion.`;
  }

  document.querySelector(".btn-signout").addEventListener("click", () => {
    document.cookie = "id=NONE;path=/";
    localStorage.setItem("userInfo", "");
    window.location.replace("../index.html");
  });
};
