// This file is automatically updated by the CI/CD service provider.
// Do not change its contents and expect to make a difference!

export const SITE_NAME = "MyNHS";
export const OUTAGE = false;
export const RELAY_URL = "https://example.com";
export const SUBDOMAIN = "default";
